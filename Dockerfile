FROM python:3.7
WORKDIR /app
ENV PYTHONPATH /app

RUN apt update -q
RUN apt install -y build-essential automake pkg-config libtool libffi-dev libgmp-dev libsecp256k1-dev postgresql-client git make curl && \
    rm -rf /var/cache/apk/*

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
