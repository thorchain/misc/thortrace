import os
import json
import logging

from flask import Flask, jsonify, request, abort
from flask.json import JSONEncoder

from steps import step1, step2, step3, step4, step5

app = Flask(__name__)

@app.route("/ping")
def ping():
    return "pong"

@app.route("/trace")
def trace():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    result = {}
    for func in [step1, step2, step3, step4, step5]:
        result = func(chain, txid)
        if not result['success']:
            break

    return json.dumps(result, indent=2, sort_keys=True)


@app.route("/steps")
def steps():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    result = []

    for func in [step1, step2, step3, step4, step5]:
        resp = func(chain, txid)
        result.append(resp)
        print(resp)
        if not resp['success']:
            break

    return json.dumps(result, indent=2, sort_keys=True)

@app.route("/step/1")
def api_step1():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    return step1(chain, txid)

@app.route("/step/2")
def api_step2():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    return step2(chain, txid)

@app.route("/step/3")
def api_step3():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    return step3(chain, txid)

@app.route("/step/4")
def api_step4():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    return step4(chain, txid)

@app.route("/step/5")
def api_step5():
    txid = request.args.get("txid", default=None, type=str)
    chain = request.args.get("chain", default=None, type=str)

    if txid is None:
        return {"error": "must have 'txid' query arg"}
    if chain is None:
        return {"error": "must have 'chain' query arg"}

    return step5(chain, txid)
