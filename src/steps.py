import math

from thorchain import Thorchain
from blockchair import Blockchair
from midgard import Midgard

# validate to address
def step1(chain, txid):
    result = { "description": "Validating to send to address...", "success": False }

    thor = Thorchain()
    try:
        tx = thor.get_tx(txid)
        result['success'] = True
    except:
        # did not detect the transaction in THORNode yet, check individual blockchains

        # TODO: add support for more chains, other than Bitcoin
        if chain.lower() != "btc":
            result['success'] = True
            result['skip'] = True
            result['message'] = f"Skipping check, {chain} not yet supported"

        chair = Blockchair()
        tx = chair.get_tx(chain, txid)
        inbound_addresses = thor.inbound_addresses(chain)

        for addr in inbound_addresses:
            if addr == tx['to_address']:
                result['success'] = True
        if not result['success']:
            result['error'] = f"Transaction sent to WRONG address! {tx['to_address']} is not a valid Asgard address: {', '.join(inbound_addresses)}"

    if result['success']:
        result['message'] = "Sent to correct address!"
    return result


# conf count check
def step2(chain, txid):
    result = { "description": "Confirmation count check", "success": False }

    thor = Thorchain()
    try:
        tx = thor.get_tx(txid)

        tip = thor.last_block_height(chain)
        if tip >= tx['tx']['finalise_height']:
            result['success'] = True
        else:
            result['error'] = f"Awaiting confirmation: {tx['tx']['finalise_height'] - tip} blocks remaining"
    except:
        result['error'] = f"Count not find transaction in THORChain"

    if result['success']:
        result['message'] = "Confirmation count reached!"
    return result

# check if transaction has been observed
def step3(chain, txid):
    result = { "description": "Check transaction has been observed", "success": False }

    thor = Thorchain()
    tx = thor.get_tx(txid)
    node_count = thor.active_node_count()
    consensus = math.ceil(node_count * 2 / 3)

    if len(tx['txs'][0]['signers']) >= consensus:
        result['success'] = True
        result['message'] = "The inbound transaction has been observed"
    else:
        result['error'] = "Not enough nodes have observed the transaction to reach a consensus"

    return result

# status
def step4(chain, txid):
    result = { "description": "Check transaction status", "success": False }

    thor = Thorchain()
    tx = thor.get_tx(txid)

    # check if Add Liquidity
    if tx['tx']['tx']['memo'].lower().startswith("add:") or tx['tx']['tx']['memo'].lower().startswith("+:"):
        result['skip'] = True
        result['message'] = "Transaction has been processed!"
        return result

    if tx['actions'] is not None:
        result['actions'] = tx['actions']
        result['message'] = "Transaction has been processed!"
        result['success'] = True
    else:
        result['error'] = "Transaction is awaiting to be processed"
    return result


# check outbound transaction has been observed
def step5(chain, txid):
    result = { "description": "Check outbound transaction(s)", "success": False }

    thor = Thorchain()
    tx = thor.get_tx(txid)
    
    if tx['tx']['tx']['memo'].lower().startswith("add:") or tx['tx']['tx']['memo'].lower().startswith("+:"):
        result['skip'] = True
        result['message'] = "No outbound transactions needed"
        return result

    if tx['out_txs'] is not None:
        result['out_txs'] = tx['out_txs']

        if tx['out_txs'][0]['memo'].startswith("REFUND"):
            midgard = Midgard()
            tx = midgard.get_tx(txid)
            result['error'] = f"Transaction was refunded due to error: {tx['actions'][0]['metadata']['refund']['reason']}"
        else:
            result['message'] = "Transaction Complete!"
            result['success'] = True


    else:
        result['error'] = "Outbound transaction(s) are pending"

    return result
