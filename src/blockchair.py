from common import HttpClient


class Blockchair(HttpClient):
    """
    A local simple implementation of blockchair
    """
    def __init__(self):
        self.base_url = "https://api.blockchair.com/"

    def get_chain(self, chain):
        if chain.lower() == "btc" or chain.lower() == "bitcoin":
            return "bitcoin"
        if chain.lower() == "bch" or chain.lower() == "bitcoin-cash":
            return "bitcoin-cash"
        if chain.lower() == "eth" or chain.lower() == "ethereum":
            return "ethereum"
        if chain.lower() == "ltc" or chain.lower() == "litecoin":
            return "litecoin"
        return None

    def get_stats(self, chain):
        chain = self.get_chain(chain)
        data = self.fetch("/stats")
        data = data['data']
        return data[chain]

    def get_tx(self, chain, hash_id):
        # rename chain to blockchain
        chain = self.get_chain(chain)
        data = self.fetch(f"/{chain}/raw/transaction/{hash_id}")
        data = data['data'][hash_id.lower()]
        return {
                "from_address": "",
                "to_address": data['decoded_raw_transaction']['vout'][0]['scriptPubKey']['addresses'][0],
                "amount": data['decoded_raw_transaction']['vout'][0]['value'],
        }
