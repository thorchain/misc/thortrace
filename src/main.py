import logging
import os
import sys
import time

from api import app
from binance import Binance
from thorchain import Thorchain

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)

def main():
    logging.info("Starting...")

    port = int(os.environ.get("PORT", 5000))
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
    app.run(host="0.0.0.0", port=port)


if __name__ == "__main__":
    main()
