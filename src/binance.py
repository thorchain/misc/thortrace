from common import HttpClient


class Binance(HttpClient):
    """
    A local simple implementation of binance chain
    """
    def __init__(self):
        self.base_url = "https://dex.binance.org/api/v1"

    def get_tx(self, hash_id):
        return self.fetch("/tx/" + hash_id + "?format=json")
