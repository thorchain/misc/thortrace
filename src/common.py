#!/usr/bin/env python
import json
import logging
import time

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


def retry(func, args=None, kwargs=None, max_tries=10):
    pass_on_args = args if args else []
    pass_on_kwargs = kwargs if kwargs else {}
    for i in range(max_tries):
        try:
            return func(*pass_on_args, **pass_on_kwargs)
            break
        except Exception as e:
            logging.error(f"retry failure ({i}): {e}")
            time.sleep(i)
            continue


def requests_retry_session(
    retries=6, backoff_factor=1, status_forcelist=(500, 502, 504), session=None,
):
    """
    Creates a request session that has auto retry
    """
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session


class HttpClient:
    """
    An generic http client
    """

    def __init__(self, base_url):
        self.base_url = base_url

    def get_url(self, path):
        """
        Get fully qualified url with given path
        """
        return self.base_url + path

    def fetch(self, path, args={}):
        """
        Make a get request
        """
        url = self.get_url(path)
        resp = requests.get(url, params=args)
        resp.raise_for_status()
        return resp.json()

    def post(self, path, payload={}):
        """
        Make a post request
        """
        url = self.get_url(path)
        resp = retry(requests_retry_session().post, [url], {"json": payload})
        if resp is None:
            raise Exception("post request has 'None' response")
        if resp.status_code != 200:
            logging.error(resp.text)
        resp.raise_for_status()
        if len(resp.text) == 0:
            return []
        return json.loads(resp.text)
