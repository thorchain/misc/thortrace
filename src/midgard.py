from common import HttpClient


class Midgard(HttpClient):
    """
    A local simple implementation of midgard
    """
    def __init__(self):
        self.base_url = "https://midgard.thorchain.info/v2"

    def get_tx(self, hash_id):
        return self.fetch(f"/actions?txid={hash_id}&limit=1&offset=0")
