from common import HttpClient


class Thorchain(HttpClient):
    """
    A local simple implementation of thorchain chain
    """
    def __init__(self):
        self.base_url = "http://thornode.thorchain.info"

    def get_tx(self, hash_id):
        return self.fetch(f"/thorchain/tx/{hash_id}/signers")

    def active_node_count(self, height=None):
        count = 0
        if height is None:
            data = self.fetch(f"/thorchain/nodes")
        else:
            data = self.fetch(f"/thorchain/nodes?height={height}")
        for node in data:
            if node['status'] == "Active":
                count += 1
        return count

    def last_block_height(self, chain):
        data = self.fetch(f"/thorchain/lastblock/{chain}")
        return data[0]['last_observed_in']

    def inbound_addresses(self, chain):
        data = self.fetch("/thorchain/vaults/asgard")
        addresses = []

        for vault in data:
            for addr in vault['addresses']:
                if chain.lower() == addr['chain'].lower():
                    addresses.append(addr['address'])

        return addresses
